<?php

namespace Drupal\search_api_nuclia\Plugin\search_api\data_type;

use Drupal\search_api\DataType\DataTypePluginBase;

/**
 * Provides a not stemmed full text data type.
 *
 * @SearchApiDataType(
 *   id = "nuclia_file",
 *   label = @Translation("Nuclia file"),
 *   description = @Translation("Nuclia file resource field"),
 *   fallback_type = "text",
 * )
 */
class NucliaFile extends DataTypePluginBase {
}
