<?php

namespace Drupal\search_api_nuclia\Plugin\views\pager;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\pager\Full;

/**
 * The plugin to handle full pager.
 *
 * @ingroup views_pager_plugins
 *
 * @ViewsPager(
 *   id = "nuclia_pager",
 *   title = @Translation("Nuclia pager"),
 *   short_title = @Translation("Nuclia pager"),
 *   help = @Translation("Nuclia pager"),
 *   theme = "search_api_nuclia_pager",
 *   register_theme = FALSE
 * )
 */
class NucliaPager extends Full {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['quantity'] = ['default' => 3];
    unset($options['tags']['contains']['last']);
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    // Remove full pager options.
    unset($form['tags']['last']);
    unset($form['quantity']);
  }

  /**
   * {@inheritdoc}
   */
  public function render($input) {
    // The 0, 1, 3, 4 indexes are correct. See the template_preprocess_pager()
    // documentation.
    $tags = [
      0 => $this->options['tags']['first'],
      1 => $this->options['tags']['previous'],
      3 => $this->options['tags']['next'],
      4 => $this->options['tags']['last'] ?? NULL,
    ];
    return [
      '#theme' => 'search_api_nuclia_pager',
      '#tags' => $tags,
      '#element' => $this->options['id'],
      '#parameters' => $input,
      '#quantity' => 3,
      '#route_name' => !empty($this->view->live_preview) ? '<current>' : '<none>',
    ];
  }

}
