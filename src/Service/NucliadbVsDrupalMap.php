<?php

namespace Drupal\search_api_nuclia\Service;

use Drupal\Core\Database\Connection;

/**
 * Nucliadb versus Drupal Map.
 *
 * This class allows to handle mapping information between Drupal entities and
 * NucliaDB resources.
 */
class NucliadbVsDrupalMap {
  public const RID_COL_NAME = 'nucliadb_rid';
  public const DRUPAL_KEY_COL_NAME = 'drupal_key';

  /**
   * Drupal database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $connection;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected DebugLogStacker $debugLogStacker;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   Drupal database connection.
   * @param DebugLogStacker $debugLogStacker
   *   Debug log stacker.
   */
  public function __construct(
        Connection $connection,
        DebugLogStacker $debugLogStacker
    ) {
    $this->connection = $connection;
    $this->debugLogStacker = $debugLogStacker;
  }

  /**
   * Create an entry into search_api_nuclia_map table.
   *
   * @param string $drupalKey
   *   Search API item ID.
   * @param string $nucliadbRid
   *   NucliaDB rid.
   *
   * @return int|string
   *   internal Id of the newly created row in table
   *
   * @throws \Exception
   */
  public function createNucliadbDrupalMapInfo(
        string $drupalKey,
        string $nucliadbRid
    ) {
    return $this->connection->insert('search_api_nuclia_map')
      ->fields(['nucliadb_rid', 'drupal_key'])
      ->values(
              [
                'nucliadb_rid' => $nucliadbRid,
                'drupal_key' => $drupalKey,
              ]
          )
      ->execute();
  }

  /**
   * Retrieve a set of entries from search_api_nuclia_map.
   *
   * @param string[] $drupalKeys
   *   Array of search API item ID to retrieve.
   * @param array $resultCols
   *   Table columns to be considered in the select query.
   *
   * @return \Drupal\Core\Database\StatementInterface|int|string|null
   *   Return the result of the query execution
   *
   * @see Drupal\search\SearchQuery::execute
   */
  public function findNucliadbDrupalMapInfoFromSearchApiItemKeys(
        array $drupalKeys,
        array $resultCols = ['nucliadb_rid', 'drupal_key']
    ) {
    return $this->connection->select('search_api_nuclia_map', 'sanm')
      ->fields('sanm', $resultCols)
      ->condition('sanm.drupal_key', $drupalKeys, 'IN')
      ->execute();
  }

  /**
   * Retrieve a set of entries from search_api_nuclia_map.
   *
   * @param array $nucliadbRids
   *   NucliaDB rid of lines to be deleted.
   * @param array $resultCols
   *   Table columns to be considered in the select query.
   *
   * @return \Drupal\Core\Database\StatementInterface|int|string|null
   *   DB Query Response.
   */
  public function findNucliadbDrupalMapInfoFromNucliadbRids(
        array $nucliadbRids,
        array $resultCols = ['nucliadb_rid', 'drupal_key']
    ) {
    return $this->connection->select('search_api_nuclia_map', 'sanm')
      ->fields('sanm', $resultCols)
      ->condition('sanm.nucliadb_rid', $nucliadbRids, 'IN')
      ->execute();
  }

  /**
   * Retrieve all entries from search_api_nuclia_map.
   *
   * @param array $resultCols
   *   Table columns to be considered in the select query.
   *
   * @return \Drupal\Core\Database\StatementInterface|int|string|null
   *   DB Query Response.
   */
  public function findAllNucliadbDrupalMapInfo(
    array $resultCols = ['nucliadb_rid', 'drupal_key']
  ) {
    return $this->connection->select('search_api_nuclia_map', 'sanm')
      ->fields('sanm', $resultCols)
      ->execute();
  }

  /**
   * Delete an entry from search_api_nuclia_map table.
   *
   * @param string[] $nucliadbRids
   *   Array of rid.
   *
   * @return int
   *   Number of rows deleted in table
   */
  public function deleteNucliadbDrupalMapInfoFromNucliadbRids(
        array $nucliadbRids
    ): int {
    $rowCount = 0;
    if (count($nucliadbRids) > 0) {
      $rowCount = $this->connection->delete('search_api_nuclia_map')
        ->condition('nucliadb_rid', $nucliadbRids, 'IN')
        ->execute();
      $this->debugLogStacker->appendSection(
            'Delete item in Drupal / NucliaDB map',
            [
            [
              'name' => 'resource(s) RID(s)',
              'value' => json_encode($nucliadbRids, JSON_PRETTY_PRINT),
              'type' => 'json',
            ],
            [
              'name' => 'row deleted in the search_api_nuclia_map table',
              'value' => $rowCount,
            ],
            ]
        );
    }
    return $rowCount;
  }

  /**
   * Delete an entry from search_api_nuclia_map table.
   *
   * @return int
   *   Number of rows deleted in table
   */
  public function deleteAll(): int {
    $rowCount = $this->connection->delete('search_api_nuclia_map')
      ->execute();
    $this->debugLogStacker->appendSection(
      'Delete All item in Drupal / NucliaDB map',
      [
        [
          'name' => 'row deleted in the search_api_nuclia_map table',
          'value' => $rowCount,
        ],
      ]
    );

    return $rowCount;
  }

  /**
   * Get a list of existing rids from a list of search-api index items.
   *
   * @param \Drupal\search_api\Item\ItemInterface[] $items
   *   Search api items.
   *
   * @return array
   *   Result
   *
   * @see NucliadbVsDrupalMap::getExistingRidsFromSearchItemIds
   */
  public function getExistingRidsFromSearchItems(array $items): array {
    $itemIds = array_values(
          array_map(
              function ($item) {
                  return $item->getId();
              }, $items
          )
      );
    return $this->getExistingRidsFromSearchItemIds($itemIds);
  }

  /**
   * Get a list of existing rids from a list of search-api index item ids.
   *
   * @param array $drupalKeys
   *   Array of search API item ID.
   * @param array $resultCols
   *   Table columns to be considered in the select query.
   * @param string|int|null $col
   *   Name of the main column in result.
   *   This parameter acts differently depending on type value. if type is
   *   set to "assoc", this parameter contains a column name, and the
   *   function returns an array resulting a fetchAllAssoc keyed using this
   *   column.
   *   if type is set to "col", this parameter contains a column index value
   *   and the function returns an array resulting a fetchCol containing
   *   values of the column designated by the index.
   * @param string|null $type
   *   Expected result type, can be either 'assoc', 'col' or null.
   *
   * @return array
   *   Result depending on type value
   */
  public function getExistingRidsFromSearchItemIds(
        array $drupalKeys,
        array $resultCols = [
          NucliadbVsDrupalMap::RID_COL_NAME,
          NucliadbVsDrupalMap::DRUPAL_KEY_COL_NAME,
        ],
        $col = 'drupal_key',
        ?string $type = 'assoc'
    ): array {
    if ($type === 'assoc' && is_string($col)) {
      return $this->findNucliadbDrupalMapInfoFromSearchApiItemKeys($drupalKeys, $resultCols)->fetchAllAssoc($col);
    }
    elseif ($type === 'col' && is_int($col)) {
      return $this->findNucliadbDrupalMapInfoFromSearchApiItemKeys($drupalKeys, $resultCols)->fetchCol($col);
    }
    elseif ($type === NULL && $col === NULL) {
      return $this->findNucliadbDrupalMapInfoFromSearchApiItemKeys($drupalKeys, $resultCols)->fetchAll();
    }
    else {
      throw new \LogicException('Inconsistent function parameters.');
    }
  }

  /**
   * Get a list of existing search-api item ids(col. drupal_key)
   *
   * @param array $rids
   *   List of rids to select.
   *
   * @return array|null
   *   Result
   */
  public function getSearchItemIdsFromRids(array $rids): ?array {
    return $this->findNucliadbDrupalMapInfoFromNucliadbRids($rids, ['drupal_key'])->fetchCol(0);
  }

  /**
   * Get all existing search-api item ids(col. drupal_key)
   *
   * @return array|null
   *   Result
   */
  public function getAllSearchItemInfo(): ?array {
    return $this->findAllNucliadbDrupalMapInfo()->fetchAll();
  }

}
