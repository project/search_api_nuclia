<?php

namespace Drupal\search_api_nuclia\Service;

use Drupal\Core\Render\Renderer;
use Psr\Log\LoggerInterface;

/**
 * Debug utility class to stack multiple log traces in one single drupal log.
 */
class DebugLogStacker {

  /**
   * Logger object.
   *
   * @var \Psr\Log\LoggerInterface|null
   */
  protected LoggerInterface $logger;

  /**
   * Log sections.
   *
   * @var array
   */
  protected ?array $sections;

  /**
   * Debug enabled state.
   *
   * @var bool
   */
  protected bool $isEnabled;

  /**
   * Renderer service.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected Renderer $renderer;

  /**
   * Constructor.
   */
  public function __construct(
        LoggerInterface $logger,
        Renderer $renderer
    ) {
    $this->logger = $logger;
    $this->renderer = $renderer;
    $this->sections = NULL;
    $this->isEnabled = FALSE;
  }

  /**
   * Get the logger instance.
   *
   * @return \Psr\Log\LoggerInterface|null
   *   Logger service.
   */
  public function getLogger(): LoggerInterface {
    return $this->logger;
  }

  /**
   * Start a new debug entry.
   */
  public function start() {
    if (!$this->isEnabled) {
      return;
    }
    if ($this->sections !== NULL) {
      throw new \LogicException('Log stack must be finished before starting.');
    }
    $this->sections = [];
  }

  /**
   * Append a new section in debug entry.
   *
   * @param string $title
   *   Section title.
   * @param array $values
   *   Exposed values.
   * @param string|null $context
   *   Context string.
   * @param string|null $subtitle
   *   Subtitle.
   */
  public function appendSection(
        string $title,
        array $values = [],
        string $context = NULL,
        string $subtitle = NULL
    ) {
    if (!$this->isEnabled) {
      return;
    }
    if (!array_key_exists('type', $values)) {
      $values = array_map(
            function ($value) {

                // @todo replace with php 8.0 match on module's php 7.4 compatibility removal.
                $type = is_array($value['value']) ? 'array'
                : (is_array($value['value']) ? 'bool'
                : (is_object($value['value']) ? 'object'
                : (is_numeric($value['value']) ? 'numeric'
                : (is_string($value['value']) ? 'string'
                  : 'unknown'
                )
                )
                )
                );
                return $value + ['type' => $type];
            }, $values
        );
    }
    $section = [
      'title' => $title,
      'values' => $values,
    ];
    if ($subtitle !== NULL) {
      $section['sub_title'] = $subtitle;
    }
    if ($context !== NULL) {
      $section['context'] = $context;
    }
    $this->sections[] = $section;
  }

  /**
   * Stop current debug entry.
   */
  public function stop() {
    if (!$this->isEnabled) {
      return;
    }
    if (is_array($this->sections)) {
      $element = [
        '#theme' => 'search_api_nuclia_debug',
        '#sections' => $this->sections,
      ];
      $renderedElement = $this->renderer->renderPlain(
            $element
        );
      $this->logger->debug(
            $renderedElement
        );
      $this->sections = NULL;
    }
    else {
      throw new \LogicException('Log stack must be started before finishing.');
    }
  }

  /**
   * Enable or disable debugging.
   *
   * @param bool $isEnabled
   *   Allow to switch on/off debugging.
   */
  public function setIsEnabled(bool $isEnabled) {
    $this->isEnabled = $isEnabled;
  }

  /**
   * Format a debug context string.
   *
   * @param string $functionName
   *   Target function name.
   * @param object|null $className
   *   Target object.
   * @param string|null $fileName
   *   Target Filename.
   *
   * @return string
   *   Formatted context string.
   */
  public static function formatContext(string $functionName, object $className = NULL, string $fileName = NULL) {
    return ($fileName !== NULL ? $fileName . ' ' : '') .
        ($className !== NULL ? get_class($className) . '::' : '') .
        $functionName;
  }

}
