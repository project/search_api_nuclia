<?php

namespace Drupal\search_api_nuclia\Service;

use Drupal\Core\File\FileSystem;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\file\Entity\File;
use Drupal\search_api\Item\ItemInterface;
use Psr\Log\LoggerInterface;

/**
 * Nuclia helper service.
 */
class NucliadbHelper {

  /**
   * Logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * Debug log stacker.
   *
   * @var DebugLogStacker
   */
  protected DebugLogStacker $debugLogStacker;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected EntityTypeManager $entityTypeManager;

  /**
   * File system service.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected FileSystem $fileSystem;

  /**
   * Constructor.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   Logger service.
   * @param DebugLogStacker $debugLogStacker
   *   Debug log stacker.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\File\FileSystem $fileSystem
   *   File system service.
   */
  public function __construct(
    LoggerInterface $logger,
    DebugLogStacker $debugLogStacker,
    EntityTypeManager $entityTypeManager,
    FileSystem $fileSystem) {
    $this->logger = $logger;
    $this->debugLogStacker = $debugLogStacker;
    $this->entityTypeManager = $entityTypeManager;
    $this->fileSystem = $fileSystem;
  }

  /**
   * Prepare NucliaDB resource body and files.
   *
   * @param \Drupal\search_api\Item\ItemInterface $item
   *   The Search API item to index into NucliaDb.
   * @param bool $processPublicFilesAsLink
   *   Indicates if public files can be processed as links field or not.
   * @param array $nucliaDBExistingResourceFieldsChecksums
   *   If the resource already exists from NucliaDB, this array contains fields
   *   checksums to be able to test if each file has changed or not.
   *
   * @return array
   *   Prepared resource info array
   *   PHP 8 attributes :
   *   #[ArrayShape([
   *      'body' => "array",
   *      'files' => "array",
   *   ])]
   *
   * @throws \Drupal\search_api\SearchApiException
   */
  public function prepareNucliaResourceInfo(ItemInterface $item, bool $processPublicFilesAsLink = FALSE, array $nucliaDBExistingResourceFieldsChecksums = []): array {
    $itemFields = $item->getFields();
    $entityAdapter = $item->getOriginalObject();
    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $entityAdapter->getEntity();
    $title = $entity->label();
    $resourceBody = ['title' => $title];
    $files = [];
    $checksumDebug = [];

    /** @var \Drupal\search_api\Item\FieldInterface $field */
    foreach ($itemFields as $field) {
      $type = $field->getType();
      $fieldId = $field->getFieldIdentifier();
      $fieldValues = $field->getValues();
      if (empty($fieldValues)) {
        continue;
      }
      foreach ($fieldValues as $delta => $fieldValue) {
        if ($fieldValue) {
          $targetField = NULL;
          $file = NULL;
          $nucliaFieldName = $this->formatNucliaDbFieldName($fieldId, $delta);
          $nucliaFieldGroup = $this->computeNucliaDbFieldGroupName($type);

          $checksumKey = $this->computeFieldChecksumKey($nucliaFieldName, $nucliaFieldGroup);
          $fieldValue = $this->prepareNucliaDbValue($nucliaFieldGroup, $fieldValue);
          $checksum = $fieldValue !== NULL ? md5($fieldValue) : NULL;

          // Process any field regarding its search api index data type, and
          // produce a NucliaDB equivalence into resource body or files.
          switch ($nucliaFieldGroup) {
            case 'links':
              $targetField = $this->prepareLinkField($fieldValue);
              break;

            case 'datetimes':
              $targetField = $this->prepareDatetimeField($fieldValue);
              break;

            case 'texts':
              $targetField = $this->prepareTextField($fieldValue);
              break;

            case 'files':
              $value = $fieldValue;

              $isValidUrl = !!filter_var($value, FILTER_VALIDATE_URL);

              // The field value extracted by search API is a valid URL.
              if ($isValidUrl) {
                $nucliaFieldGroup = 'links';
                $targetField = $this->prepareLinkField($value);
              }

              // The field value extracted by search API is not a valid URL.
              else {

                $field = $entity->{$fieldId};
                $fieldTargetType = $field->get($delta)->getDataDefinition()->getSettings()['target_type'];

                if ($fieldTargetType === 'file') {
                  $fieldValue = $field->get($delta)->getValue();
                  $fid = $fieldValue['target_id'];
                  /** @var \Drupal\file\Entity\File $file */
                  $file = $this->entityTypeManager->getStorage('file');
                  $fileEntity = $file->load($fid);
                  $fileInfo = $this->prepareFile($nucliaFieldName, $fileEntity);
                  $fileUri = $fileEntity->getFileUri();
                  $checksum = md5($fileInfo['data']);

                  // Process file as link.
                  if (str_starts_with($fileUri, 'public://') && $processPublicFilesAsLink === TRUE) {
                    $fileUrl = $fileEntity->createFileUrl(FALSE);
                    $nucliaFieldGroup = 'links';
                    $targetField = $this->prepareLinkField($fileUrl);
                  }

                  // Preprocess file as file.
                  else {
                    $fileInfo['md5'] = $checksum;
                    $file = $fileInfo;
                  }
                }
              }
              break;

            default:
              throw new \LogicException(strtr('Unhandled type: "@type"', ['@type' => $type]));
          }

          // If checksum already exist.
          $mustCreateOrUpdate = FALSE;
          if (!array_key_exists($checksumKey, $nucliaDBExistingResourceFieldsChecksums)) {
            $mustCreateOrUpdate = TRUE;
            $checksumDebug[] = [
              'name' => $checksumKey,
              'value' => strtr(
                  'Field must be created: Checksum "@checksum" not found.)', [
                    '@checksum' => $checksum,
                  ]
              ),
            ];
          }

          // If checksum does not already exist.
          elseif ($nucliaDBExistingResourceFieldsChecksums[$checksumKey] !== $checksum) {
            $mustCreateOrUpdate = TRUE;
            $checksumDebug[] = [
              'name' => $checksumKey,
              'value' => strtr(
                  'Field must be updated: Checksums "@checksum" vs "@existing_checksum" do not match.', [
                    '@checksum' => $checksum,
                    '@existing_checksum' => $nucliaDBExistingResourceFieldsChecksums[$checksumKey],
                  ]
              ),
            ];
          }
          else {
            $checksumDebug[] = [
              'name' => $checksumKey,
              'value' => strtr(
                  'Field was not modified : Checksum "@checksum" found.', [
                    '@checksum' => $checksum,
                  ]
              ),
            ];
          }

          if ($mustCreateOrUpdate) {

            // Append to resource body.
            if ($nucliaFieldGroup !== NULL && $targetField !== NULL) {
              $resourceBody[$nucliaFieldGroup][$nucliaFieldName] = $targetField;
            }
            elseif ($file !== NULL) {
              $files[] = $file;
            }
          }
        }
      }
    }

    $this->debugLogStacker->appendSection(
      'Check fields checksums', [
        [
          'name' => 'Checksum analysis',
          'value' => $checksumDebug,
          'type' => 'sub-list',
        ],
      ]
    );

    return [
      'body' => $resourceBody,
      'files' => $files,
    ];
  }

  /**
   * Compute field checksum key.
   *
   * @param string $nucliaDbFieldName
   *   Nuclia DB field name.
   * @param string $nucliaDbFieldGroup
   *   Nuclia DB field group name.
   *
   * @return string
   *   Field checksum key.
   */
  public function computeFieldChecksumKey(string $nucliaDbFieldName, string $nucliaDbFieldGroup): string {
    return $nucliaDbFieldGroup . '.' . $nucliaDbFieldName;
  }

  /**
   * Format a regular Nuclia field name.
   *
   * NB : Regular does not mean NucliaDB need this format, but mean that is the
   * normalized way for this module.
   *
   * @param string $fieldId
   *   Drupal field ID.
   * @param int $delta
   *   Field delta.
   *
   * @return string
   *   formatted field name
   */
  protected function formatNucliaDbFieldName(string $fieldId, int $delta): string {
    return $fieldId . '--' . $delta;
  }

  /**
   * Compute NucliaDb field group name from search API data type name.
   *
   * @param string $searchApiDataTypeName
   *   Search API data type name.
   *
   * @return string|null
   *   NucliaDB group name.
   */
  protected function computeNucliaDbFieldGroupName(string $searchApiDataTypeName): ?string {

    // @todo replace with php 8.0 match on module's php 7.4 compatibility removal.
    switch ($searchApiDataTypeName) {
      case 'uri':
        return 'links';

      case 'date':
        return 'datetimes';

      case 'nuclia_file':
        return 'files';

      case 'text':
      case 'string':
      case 'integer':
      case 'duration':
      case 'decimal':
        return 'texts';

      default:
        return NULL;
    }
  }

  /**
   * Prepare a value from Search API to push into nucliaDB resource field.
   *
   * @param string $nucliaFieldGroup
   *   Nuclia field group.
   * @param mixed $fieldValue
   *   Field value.
   *
   * @return mixed
   *   Prepared value.
   */
  protected function prepareNucliaDbValue(string $nucliaFieldGroup, $fieldValue) {

    // @todo replace with php 8.0 match on module's php 7.4 compatibility removal.
    switch ($nucliaFieldGroup) {
      case 'links':
      case 'datetimes':
      case 'texts':
      case 'files':
        return $fieldValue . '';

      default:
        return NULL;
    }
  }

  /**
   * Prepare NucliaDB text field.
   *
   * @param string $fieldValue
   *   Field value.
   *
   * @return array
   *   Text field array for resource body
   *   PHP 8 attributes :
   *   #[ArrayShape([
   *     'type' => "string",
   *     'data' => "array",
   *   ])]
   */
  protected function prepareTextField(string $fieldValue): array {
    return [
      'body' => $fieldValue,
      'format' => 'PLAIN',
    ];
  }

  /**
   * Prepare NucliaDB datetime field.
   *
   * @param string $fieldValue
   *   Field value.
   *
   * @return array
   *   Date field array for resource body
   *   PHP 8 attributes :
   *   #[ArrayShape([
   *     'type' => "string",
   *     'data' => "array",
   *   ])]
   */
  protected function prepareDatetimeField(string $fieldValue): array {
    return [
      'value' => date('c', strtotime($fieldValue)),
    ];
  }

  /**
   * Prepare NucliaDB link field.
   *
   * @param string $fieldValue
   *   Field value.
   *
   * @return array
   *   Link array for resource body
   *   PHP 8 attributes :
   *   #[ArrayShape([
   *     'type' => "string",
   *     'data' => "array",
   *   ])]
   */
  protected function prepareLinkField(string $fieldValue): array {
    return [
      'uri' => $fieldValue,
    ];
  }

  /**
   * Prepare file data.
   *
   * @param string $name
   *   Field name NucliaDB Field name.
   * @param \Drupal\file\Entity\File $fileEntity
   *   Drupal file entity.
   *
   * @return array
   *   File data
   *   #[ArrayShape([
   *     'name' => "",
   *     'data' => "false|string",
   *   ])]
   */
  protected function prepareFile(string $name, File $fileEntity): array {
    $fileUri = $fileEntity->getFileUri();
    $filePath = $this->fileSystem->realpath($fileUri);
    $fileContent = file_get_contents($filePath, 'r');
    return [
      'name' => $name,
      'data' => $fileContent,
    ];
  }

}
