<?php

namespace Drupal\search_api_nuclia\Cache;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Shards cache service.
 */
class ShardsCache {

  protected const CACHE_TIMEOUT = '+1 hour';

  /**
   * The default cache bin.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cache;

  /**
   * MyCache constructor.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The default cache bin.
   */
  public function __construct(CacheBackendInterface $cache) {
    $this->cache = $cache;
  }

  /**
   * Sets a cache with a specific id, data and type.
   *
   * @param string $key
   *   The cache key.
   * @param array $shards
   *   Shards uuid list.
   */
  public function setCache(string $key, array $shards): void {
    $cid = 'nuclia_shards:' . $key;

    $tags = [
      'nuclia_shards:' . $key,
      'nuclia_shards',
    ];

    $tags = Cache::mergeTags($tags, [$cid]);

    // Set the database cache.
    $this->cache->set($cid, $shards, (new \DateTime(static::CACHE_TIMEOUT))->getTimestamp(), $tags);

    // Set the static cache.
    $staticCache = &drupal_static(__FUNCTION__ . $cid, NULL);
    $staticCache = $shards;
  }

  /**
   * Get a specific cache.
   *
   * @param string $key
   *   The cache key.
   *
   * @return array
   *   The shards from cache or an empty array.
   */
  public function getCache(string $key): array {
    $cid = 'nuclia_shards:' . $key;

    $staticCache = &drupal_static(__FUNCTION__ . $cid, NULL);

    if ($staticCache) {
      // If the static cache exists, then return it.
      return $staticCache;
    }

    // Get the cache out of the database and return the data component.
    $result = $this->cache->get($cid);
    return $result->data ?? [];
  }

}
