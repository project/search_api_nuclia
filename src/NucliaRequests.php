<?php

namespace Drupal\search_api_nuclia;

use Drupal\search_api_nuclia\Cache\ShardsCache;
use Drupal\search_api_nuclia\Service\DebugLogStacker;
use Drupal\search_api_nuclia\Service\NucliadbHelper;
use Nuclia\Api\ResourceFieldsApi;
use Nuclia\Api\ResourcesApi;
use Nuclia\Api\SearchApi;
use Nuclia\ApiClient;
use Nuclia\Enum\Enum;
use Nuclia\Enum\ShowEnum;
use Nuclia\EnumArray\EnumArray;
use Nuclia\Headers\UploadBinaryFileHeaders;
use Nuclia\Query\GetResourceQuery;
use Nuclia\Query\SearchQuery;
use Psr\Log\LoggerInterface;

/**
 * Operate request against Nuclia\ApiClient.
 */
class NucliaRequests {
  /**
   * NucliaDB client.
   *
   * @var \Nuclia\ApiClient
   */
  protected ApiClient $apiClient;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * Debug log stacker.
   *
   * @var \Drupal\search_api_nuclia\Service\DebugLogStacker
   */
  protected DebugLogStacker $debugLogStacker;
  /**
   * NucliaDB Resources API.
   *
   * @var \Nuclia\Api\ResourcesApi
   */
  protected ResourcesApi $resourcesApi;

  /**
   * NucliaDB Resource fields API.
   *
   * @var \Nuclia\Api\ResourceFieldsApi
   */
  protected ResourceFieldsApi $resourceFieldsApi;

  /**
   * NucliaDB Search API.
   *
   * @var \Nuclia\Api\SearchApi
   */
  protected SearchApi $searchApi;

  /**
   * NucliaDB helper.
   *
   * @var \Drupal\search_api_nuclia\Service\NucliadbHelper
   */
  protected NucliadbHelper $nucliadbHelper;

  /**
   * Shards cache service.
   *
   * @var \Drupal\search_api_nuclia\Cache\ShardsCache
   */
  protected ShardsCache $shardsCache;

  /**
   * Constructor.
   *
   * @param string $zone
   *   NucliaDB zone setting.
   * @param string $token
   *   NucliaDB token setting.
   * @param string $kbid
   *   NucliaDB knowledge base ID setting.
   * @param \Psr\Log\LoggerInterface $logger
   *   Logger service.
   * @param \Drupal\search_api_nuclia\Service\DebugLogStacker $debugLogStacker
   *   Debug log stacker.
   * @param \Drupal\search_api_nuclia\Service\NucliadbHelper $nucliadbHelper
   *   NucliaDB helper.
   */
  public function __construct(
        string $zone,
        string $token,
        string $kbid,
        LoggerInterface $logger,
        DebugLogStacker $debugLogStacker,
        NucliadbHelper $nucliadbHelper
    ) {
    $this->apiClient = new ApiClient($zone, $token, $kbid);

    $this->logger = $logger;
    $this->debugLogStacker = $debugLogStacker;
    $this->nucliadbHelper = $nucliadbHelper;
    $this->shardsCache = \Drupal::service('search_api_nuclia.shards_cache');

    $this->resourcesApi = $this->apiClient->createResourcesApi();
    $this->resourceFieldsApi = $this->apiClient->createResourceFieldsApi();
    $this->searchApi = $this->apiClient->createSearchApi();
  }

  /**
   * Resource Update request.
   *
   * @param string $rid
   *   Resource ID to update.
   * @param array $body
   *   Resource Update request body.
   *
   * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
   */
  public function updateResource(
        string $rid,
        array $body
    ) {
    $response = $this->resourcesApi->modifyResource($rid, $body);
    $responseCode = $response->getStatusCode();
    if ($responseCode === 200) {
      $this->debugLogStacker->appendSection(
            'Update item in NucliaDB',
            [
            ['name' => 'Resource RID', 'value' => $rid],
            [
              'name' => 'Request body',
              'value' => json_encode($body, JSON_PRETTY_PRINT),
              'type' => 'json',
            ],
            ],
            DebugLogStacker::formatContext(__FUNCTION__, $this)
        );
    }
    else {
      $this->logger->alert(
        'NucliaDB returned an error (HTTP Code @code) while updating resource.', [
          '@code' => $responseCode,
        ]
      );
    }
  }

  /**
   * Resource creation request.
   *
   * @param array $body
   *   Resource creation request body.
   *
   * @return string|null
   *   Return rid or null if the nuclia call return an error response code.
   *
   * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
   * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
   * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
   * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
   */
  public function createResource(
        array $body
    ): ?string {
    $newRid = NULL;
    $response = $this->resourcesApi->createResource($body);
    $responseCode = $response->getStatusCode();

    // Init debug info.
    $debugValues = [
      [
        'name' => 'Request body',
        'value' => json_encode($body, JSON_PRETTY_PRINT),
        'type' => 'json',
      ],
      ['name' => 'Http response code', 'value' => $responseCode],
    ];

    if ($responseCode === 201) {
      $responseContent = json_decode($response->getContent());
      $newRid = $responseContent->uuid;

      // Append debug info.
      $debugValues = [
        ...$debugValues,
        ['name' => 'Created ressource RID', 'value' => $newRid],
        [
          'name' => 'Full response content',
          'value' => json_encode($responseContent, JSON_PRETTY_PRINT),
          'type' => 'json',
        ],
      ];
    }
    else {
      $this->logger->alert(
        'NucliaDB returned an error (HTTP Code @code) while creating resource.', [
          '@code' => $responseCode,
        ]
      );
    }

    // Output debug Log.
    $this->debugLogStacker->appendSection(
          'Create item in NucliaDB',
          $debugValues,
          DebugLogStacker::formatContext(__FUNCTION__, $this)
      );

    return $newRid;
  }

  /**
   * Get resources field MD5 checksums for any field.
   *
   * @param string $rid
   *   NucliaDB Resource ID.
   *
   * @return array
   *   Array of fields' md5 checksum keyed by
   *
   * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
   * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
   * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
   * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
   */
  public function getResourceFieldsChecksums(string $rid):array {
    $result = [];

    // Get resource.
    $showParam = EnumArray::show([Enum::show(ShowEnum::VALUES)]);
    $response = $this->resourcesApi->getResource($rid, (new GetResourceQuery())->setShow($showParam));
    $responseCode = $response->getStatusCode();

    $debugValues = [
      ['name' => 'Resource RID', 'value' => $rid],
    ];
    $requestDebugValues = [
      [
        'name' => '"show" parameter value',
        'value' => json_encode($showParam->getValues(), JSON_PRETTY_PRINT),
        'type' => 'json',
      ],
      ['name' => 'Http response code', 'value' => $responseCode],
    ];
    if ($responseCode === 200) {
      $responseContent = json_decode($response->getContent());

      $data = (array) $responseContent->data;
      foreach ($data as $dataGroupName => $dataGroupValue) {
        foreach ((array) $dataGroupValue as $fieldName => $fieldValues) {

          // If the field belongs to "files" group, md5 checksum is located in
          // a specific place.
          if ($dataGroupName === 'files') {
            $md5Checksum = $fieldValues->value->file->md5 ?? NULL;
          }
          else {
            $md5Checksum = $fieldValues->value->md5 ?? NULL;
          }

          $result[$this->nucliadbHelper->computeFieldChecksumKey($fieldName, $dataGroupName)] = $md5Checksum;
        }
      }
      $requestDebugValues = [
        ...$requestDebugValues,
        [
          'name' => 'Full response content',
          'value' => json_encode($responseContent, JSON_PRETTY_PRINT),
          'type' => 'json',
        ],
      ];
      $debugValues = [
        ...$debugValues,
        [
          'name' => 'Get resource request',
          'value' => $requestDebugValues,
          'type' => 'sub-list',
        ],
        [
          'name' => 'MD5 field checksums',
          'value' => json_encode($result, JSON_PRETTY_PRINT),
          'type' => 'json',
        ],
      ];
    }

    // Output debug Log.
    $this->debugLogStacker->appendSection(
          'Get resource fields checksums',
          $debugValues,
          DebugLogStacker::formatContext(__FUNCTION__, $this)
      );

    return $result;
  }

  /**
   * Resources deletion request.
   *
   * @param string $rid
   *   Resource ID to delete.
   */
  public function deleteResource(
        string $rid
    ) {
    $this->resourcesApi->deleteResource($rid);
    $this->debugLogStacker->appendSection(
          'Delete item in NucliaDB',
          [
          ['name' => 'resource RID', 'value' => $rid],
          ],
          DebugLogStacker::formatContext(__FUNCTION__, $this)
      );
  }

  /**
   * File uploading request.
   *
   * @param string $rid
   *   Target resource ID.
   * @param string $fieldId
   *   Field ID for the file in resource.
   * @param string $data
   *   File data.
   * @param string $md5
   *   File data md5 checksum.
   *
   * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
   */
  public function addFileField(
        string $rid,
        string $fieldId,
        string $data,
        string $md5
    ) {

    $headers = (new UploadBinaryFileHeaders())
      // Setting X-MD5 Header is a workaround: NucliaDB would normally
      // calculate md5 checksum itself, but it does not work.
      // upload-binary-file api call offers ability to define file's md5
      // checksum using a specific http header. So we use this temporary
      // trick waiting for a NucliaDB proper fix.
      ->setMd5($md5);

    $response = $this->resourceFieldsApi->uploadBinaryFile($rid, $fieldId, $data, $headers);
    $responseCode = $response->getStatusCode();
    if ($responseCode === 201) {
      $this->debugLogStacker->appendSection(
        'uploading file data',
          [
            ['name' => 'Nuclia field name', 'value' => $fieldId],
            ['name' => 'length (bytes)', 'value' => strlen($data)],
            ['name' => 'resource RID', 'value' => $rid],
          ],
          DebugLogStacker::formatContext(__FUNCTION__, $this)
        );
    }
    else {
      $this->logger->alert(
        'NucliaDB returned an error (HTTP Code @code) while uploading file data (@data_length bytes) in field "@name" of resource @rid.', [
          '@code' => $responseCode,
          '@rid' => $rid,
          '@name' => $fieldId,
          '@data_length' => strlen($data),
        ]
      );
    }
  }

  /**
   * Search request.
   *
   * @param string $query
   *   Search query.
   * @param int $pageSize
   *   Page size.
   * @param int $pageNumber
   *   Page number.
   *
   * @return string[]|null
   *   Array of rids matching the search parameters
   *
   * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
   * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
   * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
   * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
   */
  public function search(string $query, int $pageSize, int $pageNumber): ?array {
    $resultRids = [];
    $nextPage = FALSE;

    // Get shards from cache if any.
    $shards = $this->shardsCache->getCache($query);

    // Nuclia search request.
    $response = $this->searchApi->search(
          (new SearchQuery())
            ->setQuery($query)
            ->setPageSize($pageSize)
            ->setPageNumber($pageNumber)
            ->setShards($shards)
      );
    $responseCode = $response->getStatusCode();

    // If response was successful.
    if ($responseCode === 200) {

      // Get response content.
      $responseContent = json_decode($response->getContent());

      // Get results.
      $ridRegistry = [];
      $nextPage = (int) $responseContent->paragraphs->next_page;
      $responseShards = $responseContent->shards;
      $this->shardsCache->setCache($query, $responseShards);
      $resultRids = array_reduce(
            $responseContent->paragraphs->results,
            function (array $carry, object $item) use (&$ridRegistry) {
                $rid = $item->rid;
              if (!array_key_exists($rid, $ridRegistry)) {
                    $ridRegistry[$rid] = ($ridRegistry[$rid] ?? 0) + $item->score;
                    $carry[] = $rid;
              }
                return $carry;
            }, []
        );

      // Log debug.
      $this->debugLogStacker->appendSection(
            'NucliaDB search request',
            [
            ['name' => 'Query' , 'value' => $query],
            ['name' => 'Page size' , 'value' => $pageSize],
            ['name' => 'Page number' , 'value' => $pageNumber],
            [
              'name' => 'Retrieved shards',
              'value' => json_encode($shards, JSON_PRETTY_PRINT),
              'type' => 'json',
            ],
            ['name' => 'Has next page', 'value' => $nextPage],
            [
              'name' => 'RIDs',
              'value' => json_encode($resultRids, JSON_PRETTY_PRINT),
              'type' => 'json',
            ],
            [
              'name' => 'Full response content',
              'value' => json_encode($responseContent, JSON_PRETTY_PRINT),
              'type' => 'json',
            ],
            ],
            DebugLogStacker::formatContext(__FUNCTION__, $this)
        );
    }

    // If response failed.
    else {

      // Log error.
      $this->logger->alert(
        'NucliaDB search request returned an error (HTTP Code @code)<br>query: "@query", <br>pageSize: "@pageSize", <br>pageNumber: "@pageNumber"', [
          '@code' => $responseCode,
          '@query' => $query,
          '@pageSize' => $pageSize,
          '@pageNumber' => $pageNumber,
        ]);
    }
    return [
      'result_rids' => $resultRids,
      'next_page' => $nextPage,
    ];
  }

  /**
   * Set synchronized mode.
   *
   * @param bool $switch
   *   Switch synchronized mode on/off.
   */
  public function setSynchronizedMode(bool $switch) {
    if ($switch === TRUE) {
      $this->apiClient->addDefaultHeader('X-SYNCHRONOUS', 'true');
      return;
    }
    $this->apiClient->removeDefaultHeader('X-SYNCHRONOUS');
  }

}
