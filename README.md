
# About Nuclia
How does Nuclia bring your Drupal search to the next level?

- Nuclia’s semantic AI is able to “understand” what your users are looking for.
- Nuclia’s AI replaces the complexity of rule-based searching and offers a better experience to users.
- Nuclia offers better and more relevant results to users by mixing in a “single” result, keyword results and semantic results.
- If a user doesn’t know the exact keyword to search for, they can just describe what they want, and get optimized results.

# About this module

This module allows you to use Nuclia as your search engine in your Drupal website.
It integrates with the Drupal's search API.

[See video demo here](https://www.youtube.com/watch?v=qC4USH_qxrQ)

For now, this module integrates a subset of Nuclia capabilities but aims to evolve quickly to much higher features.

# Requirements

This module requires :
 * a Nuclia account. You can sign up for a free trial at [https://nuclia.com](https://nuclia.com).
 * the Search API module (https://www.drupal.org/project/search_api)
 * the Nuclia PHP client library: https://github.com/makinacorpus/nucliadb-php-client <br>
   added to your Drupal project using composer<br>
   `composer require makina-corpus/nucliadb-php-client`

# Installing module
* Download the module and all of its dependencies using composer:
  `composer require drupal/search_api_nuclia`
* Enable the module's dependencies on the modules admin page url (/admin/modules).


Configuring search API server and index
---------------------------------------

* On the Search API administration page (/admin/config/search/search-api), add
  a new server, select backend "NucliaDB", and check the "Enabled" checkbox.

* In the "Configure NucliaDB backend" section, fill in "Zone", "Token" and
  "Knowledge base Identifier" field using information found in your Nuclia cloud
  account on https://nuclia.cloud

* On the Search API administration page (/admin/config/search/search_api), add
  a new index, enable it and select the server you just created in the previous
  section.

* On the "Fields" tab of your index
  (/admin/config/search/search_api/index/[YOUR INDEX NAME]/fields), check all
  the fields you want to have indexed in the Nuclia index as you usually do
  when you set up Search API index.

* On the "Processors" of your index
  (/admin/config/search/search_api/index/[YOUR INDEX NAME]/processors),
  select the processors you want to apply before the data is being sent to
  NucliaDB's servers.
